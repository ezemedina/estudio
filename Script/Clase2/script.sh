#!/bin/bash

read -p "Username: " USERNAME
read -sp "Password: " PASSWORD
echo ""

for i in $(ls | grep -v *.sh); do
    if [[ $(cat $i | awk -F':' {'print $1'} | grep $USERNAME | wc -l) -gt 0 ]]; then
        FILEPASSWORD=$(cat $i | grep $USERNAME | awk -F':' {'print $2'})
        if [[ $FILEPASSWORD = $PASSWORD ]]; then
            echo "$i\\$USERNAME"
        else 
            echo "Clave erronea para $i"
        fi
    else
        echo "Usuario no encontrado en $i"
    fi
done